package tokenService

import (
	"encoding/json"
	"fmt"
	"github.com/gabrielpsilva/glue/consts"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"
)


type TokenService struct {
	token   string
	expire  time.Time

	mux sync.Mutex

	email string
	password string
	tenantID string

}


func (ts *TokenService) Configure(email, password, tenantID string){
	ts.email = email
	ts.password = password
	ts.tenantID = tenantID
}

func (ts *TokenService) RunTokenService(){
	go ts.coreRoutine()
}

func (ts *TokenService) GetToken() (string, bool) {
	if len(ts.token) <= 0 {
		return "", false
	}

	if time.Now().Local().After(ts.expire)  {
		return "", false
	}

	ts.mux.Lock()
	defer ts.mux.Unlock()
	return ts.token, true
}

func (ts *TokenService) coreRoutine(){

	for {
		if len(ts.token) <= 0 {
			fmt.Println("\nToken refresh")
			ts.refreshToken()
		}

		if time.Now().Local().After(ts.expire) {
			fmt.Println("\nToken Expired")
			ts.refreshToken()
		}
		
		time.Sleep( time.Second * 1)
		fmt.Print(".")
	}
}



func (ts *TokenService) refreshToken() {

	login := struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}{
		Email:    ts.email,
		Password: ts.password,
	}

	loginBytes, _ := json.Marshal(login)
	loginStr := string(loginBytes)
	payload := strings.NewReader(loginStr)

	url := consts.AutherBaseURL + "/v1" + consts.AutherSigninURL + "/" + ts.tenantID

	req, _ := http.NewRequest("POST", url, payload)
	req.Header.Add("content-type", "application/json")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		logrus.Errorf("can not authenticate: %v", err)
		return
	}
	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	if res.StatusCode != 200 {
		logrus.Errorf("can not authenticate\n: %v", body)
	}

	var authData map[string]interface{}

	_ = json.Unmarshal(body, &authData)

	ts.mux.Lock()
	ts.token = authData["idToken"].(string)
	ts.mux.Unlock()
	expireInInt, _ := strconv.Atoi(authData["expiresIn"].(string))
	// Make expire 5 seconds earlies just to be safe
	ts.expire = time.Now().Local().Add(time.Second * time.Duration(expireInInt-5))
}