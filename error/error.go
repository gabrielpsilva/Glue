package error

import (
	"fmt"
	"github.com/gabrielpsilva/glue/consts"
)

// Error is the general Error format of this application
type Error struct {
	code          int
	description   string
	documentation string
}

// NewError Returns new Error struct
func NewError(code int, description, documentation string) Error {
	e := Error{}
	e.code = code
	e.description = description
	e.documentation = documentation
	if len(e.documentation)  <= 0 {
		e.documentation = consts.NoSolution
	}
	return e
}

// NewErrorWithError Returns new Error struct
func NewErrorWithError(code int, description error, documentation string) *Error {
	e := Error{}
	e.code = code
	e.description = fmt.Sprintf("%v", description)
	e.documentation = documentation
	if len(e.documentation)  <= 0 {
		e.documentation = consts.NoSolution
	}
	return &e
}

func (e *Error) Error() string {
	return fmt.Sprintf("ERROR: \ncode: %d\ndesc:%s\ndoc:%s", e.code, e.description, e.documentation)
}
