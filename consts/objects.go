package consts

const (
	ReservedObjectClass = "IOCLASS"
	ReservedErrorClass  = "IOERROR"
)

var ReservedClassNames = []string{
	ReservedObjectClass,
	ReservedErrorClass,
}

const (
	CLassFieldClass       = "className"
	CLassFieldPluralName  = "classPluralName"
	ClassFieldDescription = "classDescription"
	ClassFieldCreatedAt   = "classCreatedAt"
)

var ClassMandatoryFieldKeys = []string{
	CLassFieldClass,
	CLassFieldPluralName,
	ClassFieldDescription,
	ClassFieldCreatedAt,
}

const (
	FieldTypeText     = "TEXT"
	FiledTypeNumber   = "NUMBER"
	FiledTypeBoolean  = "BOOLEAN"
	FiledTypeDatetime = "DATETIME"
)

var FieldTypes = []string{
	FieldTypeText,
	FiledTypeNumber,
	FiledTypeBoolean,
	FiledTypeDatetime,
}
