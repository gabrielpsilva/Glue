package consts

const HTTPRequestIdHeader  = "X-Request-Id"
const GCPRequestIdHeader  = "x-cloud-trace-context"

var LookupHeaders =  []string{
	GCPRequestIdHeader,
	HTTPRequestIdHeader,
}


// Auther URL/I
// AutherBaseURL Base URL to the service
const AutherBaseURL = "https://auther-gs4lfxihta-uc.a.run.app"
// AutherTokenVerifyURI token verification path
const AutherTokenVerifyURI = "/tokenCheck"
// AutherV1SigninURL Signin path
const AutherSigninURL = "/signin"

// AutherV1TokenVerifyURI Deprecated: hard code the api version but use the constant for the rest of URI
const AutherV1TokenVerifyURI = "/v1/tokenCheck"

// system admin account
const ObaseAdmin = "obaseAdmin@axpress.ax"

