package funcs

import (
	"encoding/json"
	"fmt"
	"github.com/gabrielpsilva/glue/consts"
	"github.com/gabrielpsilva/glue/v1/ioobject"
	"net/http"
	"strings"
)

func TokenCheck(token string) (ioObjects.IoObject, bool) {

	request, _ := http.NewRequest("GET" ,
		consts.AutherBaseURL + consts.AutherV1TokenVerifyURI,
		strings.NewReader(""))
	request.Header.Set("Authorization", token)
	request.Header.Set("content-type", "application/json")

	client := &http.Client{}
	response, _ := client.Do(request)

	var result map[string]interface{}
	err := json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		msg := fmt.Sprintf("%v", err)
		ret := ioObjects.CreateError("7406", msg, consts.NoSolution)
		return ret, false
	}

	if result["Class"] == consts.ReservedErrorClass {
		b, _ := json.Marshal(result)
		var ret ioObjects.IoObject
		json.Unmarshal(b, &ret)
		return ret, false
	}

	ret := ioObjects.IoObject{}
	ret.Class = "IOAUTH"
	ret.FieldAdd("map", "Authentication", "authentication", true, result)

	return ret, true
}
