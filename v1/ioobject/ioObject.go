// Package set of functions to facilitate manipulation of a IoObject
package ioObjects

import (
	"fmt"
	"github.com/gabrielpsilva/glue/consts"
	"strconv"
	"strings"
	"time"
)


// IoObject All responses in this application and its microservices should be wrapped in thi struct
// the data is a field and the class should be declared in the database. If it is a control class
// it should be made a reserved class in the consts file of the Glue project
type IoObject struct {
	Id          string
	Class       string
	CreatedAt   time.Time
	LastChanged time.Time
	Fields      []IoField
}

// NewIoField Creates and return a new empty field. All values are initialized with go's default values
func NewIoField() IoField {
	return IoField{}
}

func (io *IoObject) FieldAdd(fieldType, label, keyField string, required bool, value interface{}) *IoField {

	f := NewIoField()
	f.Type = strings.ToUpper(fieldType)
	f.FieldKey = keyField
	f.Label = label
	f.Required = required
	f.Value = value

	io.Fields = append(io.Fields, f)
	return &io.Fields[len(io.Fields)-1]
}

func (io *IoObject) FieldDelete(fieldKey string) {

	for i := 0; i < len(io.Fields); i++ {
		if io.Fields[i].FieldKey == fieldKey {
			io.Fields[i] = io.Fields[len(io.Fields)-1]
			io.Fields = io.Fields[:len(io.Fields)-1]
		}
	}
}

func (io *IoObject) FieldGet(fieldKey string) *IoField {

	for i := 0; i < len(io.Fields); i++ {
		if io.Fields[i].FieldKey == fieldKey {
			return &io.Fields[i]
		}
	}
	return nil
}

func (io *IoObject) FieldSetValue(fieldKey string, value interface{}) {

	f := io.FieldGet(fieldKey)
	f.Value = value
}

func (io *IoObject) FieldGetExcludeClassFields() []IoField {

	ret := make([]IoField, 0)

	for i := 0; i < len(io.Fields); i++ {
		if !io.Fields[i].IsFieldClassMandatory() {
			ret = append(ret, io.Fields[i])
		}
	}
	return ret
}

func (io *IoObject) ConvertToClass(className string, pluralName string, description string) {

	io.Class = consts.ReservedObjectClass

	for _, class := range consts.ReservedClassNames {
		io.FieldDelete(class)
	}

	// Not using FieldAdd function because it does not allow to set the FieldKey with a reserved FIELD_TYPE_TEXT
	f := NewIoField()
	f.Type = consts.FieldTypeText
	f.Label = "class Name"
	f.FieldKey = consts.CLassFieldClass
	f.Value = strings.ToUpper(className)
	f.Required = true
	io.Fields = append(io.Fields, f)

	f = NewIoField()
	f.Type = consts.FieldTypeText
	f.Label = "Plural Name"
	f.FieldKey = consts.CLassFieldPluralName
	f.Value = pluralName
	f.Required = true
	io.Fields = append(io.Fields, f)

	f = NewIoField()
	f.Type = consts.FieldTypeText
	f.Label = "Description"
	f.FieldKey = consts.ClassFieldDescription
	f.Value = description
	f.Required = true
	io.Fields = append(io.Fields, f)
}

func (io *IoObject) ConvertClassToObject(from IoObject) {

	io.Class = from.FieldGet(consts.CLassFieldClass).Value.(string)
	io.Fields = from.FieldGetExcludeClassFields()
}

func CreateError(code string, description string, solution string) IoObject {
	
	io := IoObject{}
	io.Fields = []IoField{}

	io.Class = consts.ReservedErrorClass
	_ = io.FieldAdd(consts.FiledTypeNumber, "Code", "code", true, code)
	_ = io.FieldAdd(consts.FieldTypeText, "Description", "description", true, description)
	_ = io.FieldAdd(consts.FieldTypeText, "Solution", "solution", true, solution)

	return io
}
func CreateErrorWithError(code string, err error, solution string) IoObject {

	io := IoObject{}
	io.Fields = []IoField{}

	io.Class = consts.ReservedErrorClass
	_ = io.FieldAdd(consts.FiledTypeNumber, "Code", "code", true, code)
	_ = io.FieldAdd(consts.FieldTypeText, "Description", "description", true, fmt.Sprintf("%v", err))
	_ = io.FieldAdd(consts.FieldTypeText, "Solution", "solution", true, solution)

	return io
}

func (io IoObject) ToArray() []IoObject{
	var ret []IoObject
	return append(ret, io)
}

func (io *IoObject) IsClassNameReserved() bool {
	return containsInStringArray(io.Class, consts.ReservedClassNames)
}

func (io *IoObject) IsError() bool {
	if io.Class == consts.ReservedErrorClass {
		return true
	}
	return false
}

func (io *IoObject) Error() string {
	//return fmt.Sprintf("%v", io)
	return fmt.Sprintf("#{io}")
}
func (io *IoObject) ErrorShort() string {
	code := io.FieldGet("code")
	desc := io.FieldGet("description")
	return fmt.Sprintf("%s - %s", code.Value, desc.Value)
}

func IsError(array []IoObject) bool {
	if containsErrorInIoObjectArray(array) {
		return true
	}
	return false
}

func GetCodeError(array []IoObject) string {
	if len(array) > 0 {
		if IsError(array){
			return array[0].FieldGet("code").Value.(string)
		}
	}
	return ""
}

func GetCodeErrorInt(array []IoObject) int {
	code, err := strconv.Atoi(GetCodeError(array))
	if err != nil {
		return 0
	}
	return code
}
func containsErrorInIoObjectArray(array []IoObject) bool {
	for _, obj := range array {
		if obj.IsError() {
			return true
		}
	}
	return false
}


