package ioObjects

import "github.com/gabrielpsilva/glue/consts"

type IoField struct {
	Type     string      `json:"type,omitempty" bson:"type,omitempty"`
	Label    string      `json:"label,omitempty" bson:"label,omitempty"`
	FieldKey string      `json:"fieldKey,omitempty" bson:"fieldKey,omitempty"`
	Required bool        `json:"required,omitempty" bson:"required,omitempty"`
	Value    interface{} `json:"value,omitempty" bson:"value,omitempty"`
}

func (field *IoField) IsFieldTypeValid() bool {
	return containsInStringArray(field.Type, consts.FieldTypes)
}

func (field *IoField) IsFieldClassMandatory() bool {
	return containsInStringArray(field.FieldKey, consts.ClassMandatoryFieldKeys)
}

func containsInStringArray(str string, strArray []string) bool {
	for _, a := range strArray {
		if a == str {
			return true
		}
	}
	return false
}
