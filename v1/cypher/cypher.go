package cypher

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	b64 "encoding/base64"
	"io"
	"io/ioutil"
	"os"
)

func DecryptFile(filePath string) (string, error) {

	dcryptKey := os.Getenv("ENV_KEY")

	dat, err := ioutil.ReadFile(filePath)
	if err != nil {
		return "", err
	}
	return Decrypt(dat, dcryptKey), nil
}

// Decrypt will make a cipher readable
func Decrypt(cipherd []byte, keystring string) string {
	// Byte array of the string
	ciphertext := cipherd

	// Key
	key := []byte(keystring)

	// Create the AES cipher
	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	// Before even testing the decryption,
	// if the text is too small, then it is incorrect
	if len(ciphertext) < aes.BlockSize {
		panic("Text is too short")
	}

	// Get the 16 byte IV
	iv := ciphertext[:aes.BlockSize]

	// Remove the IV from the ciphertext
	ciphertext = ciphertext[aes.BlockSize:]

	// Return a decrypted stream
	stream := cipher.NewCFBDecrypter(block, iv)

	// Decrypt bytes from ciphertext
	stream.XORKeyStream(ciphertext, ciphertext)

	return string(ciphertext)
}

// Encrypt will cipher a readable
func Encrypt(plainstring, keystring string) []byte {
	// Byte array of the string
	plaintext := []byte(plainstring)

	// Key
	key := []byte(keystring)

	// Create the AES cipher
	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	// Empty array of 16 + plaintext length
	// Include the IV at the beginning
	ciphertext := make([]byte, aes.BlockSize+len(plaintext))

	// Slice of first 16 bytes
	iv := ciphertext[:aes.BlockSize]

	// Write 16 rand bytes to fill iv
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		panic(err)
	}

	// Return an encrypted stream
	stream := cipher.NewCFBEncrypter(block, iv)

	// Encrypt bytes from plaintext to ciphertext
	stream.XORKeyStream(ciphertext[aes.BlockSize:], plaintext)

	return ciphertext
}

// EncryptAndBase64 will cipher a readable and return in base64
func EncryptAndBase64(plainstring, keystring string) string {

	data := Encrypt(plainstring, keystring)
	return b64.StdEncoding.EncodeToString(data)
}

// Base64AndDecrypt will make a cipher in base64 readable
func Base64AndDecrypt(b64string, keystring string) string {

	data, err := b64.StdEncoding.DecodeString(b64string)
	if err != nil {
		panic(err)
	}
	return Decrypt(data, keystring)

}
