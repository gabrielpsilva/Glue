## Source


I created this package to allow me a quick **go get -u** in my projects

### Usage

```golang
go get -u github.com/gabrielpsilva/quickcypher
```

### Encrypt/Decrypt
```golang
Encrypt(plainstring, keystring string) []byte
Decrypt(cipherd []byte, keystring string) string
```

##### Credits to: josephspurrier  
https://gist.github.com/josephspurrier/8304f09562d81babb494