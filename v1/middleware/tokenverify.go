package middleware

import (
	"encoding/json"
	"fmt"
	"github.com/gabrielpsilva/glue/consts"
	"github.com/gabrielpsilva/glue/v1/ioobject"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)


func TokenCheck() gin.HandlerFunc {
	return func(c *gin.Context) {
		
		request, _ := http.NewRequest("GET" ,
			consts.AutherBaseURL + consts.AutherV1TokenVerifyURI,
			strings.NewReader(""))
		request.Header.Set("Authorization", c.GetHeader("Authorization"))
		request.Header.Set("content-type", "application/json")

		client := &http.Client{}
		response, _ := client.Do(request)

		var result map[string]interface{}
		err := json.NewDecoder(response.Body).Decode(&result)
		if err != nil {
			msg := fmt.Sprintf("%v", err)
			ret := ioObjects.CreateError("5406", msg, consts.NoSolution)
			c.AbortWithStatusJSON(http.StatusNotAcceptable, ret)
			return
		}
		if result["Class"] == consts.ReservedErrorClass{
			c.AbortWithStatusJSON(http.StatusNotAcceptable, result)
			return
		}
		
		c.JSON(200, result)

	}
}