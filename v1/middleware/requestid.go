package middleware

import (
	"github.com/gin-gonic/gin"
	"github.com/satori/go.uuid"
	"github.com/gabrielpsilva/glue/consts"
)

// RequestID Allows developer to send a list of possible requestID headers
func RequestID() gin.HandlerFunc {
	return func(c *gin.Context) {

		if rid := c.Request.Header.Get(consts.HTTPRequestIdHeader); rid != "" {
			return
		}
		c.Set(consts.HTTPRequestIdHeader, uuid.NewV4().String())
	}
}

// RequestIDWithOpts Allows developer to send a list of possible requestID headers
func RequestIDWithOpts(headers []string) gin.HandlerFunc {
	return func(c *gin.Context) {

		for _, opt := range headers {
			header := c.Request.Header.Get(opt)
			if header != "" {
				c.Set(consts.HTTPRequestIdHeader, header)
				return
			}
		}
		c.Set(consts.HTTPRequestIdHeader, uuid.NewV4().String())
	}}