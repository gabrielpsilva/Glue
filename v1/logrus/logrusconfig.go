package logrus

import (
	"fmt"
	"os"
	"path"
	"runtime"
	"strings"

	"github.com/sirupsen/logrus"
)

func LogrusConfig(logLevel string, jsonLog bool) {

	logrus.SetOutput(os.Stdout)
	logrus.SetLevel(getLogLevel(logLevel))
	logrus.SetReportCaller(true)


	if jsonLog {
		logrus.SetFormatter(&logrus.JSONFormatter{
			TimestampFormat: "06-01-02 15:04:05.000",
			CallerPrettyfier: func(f *runtime.Frame) (string, string) {
				_, filename := path.Split(f.File)
				_, funcname := path.Split(f.Function)
				return funcname, fmt.Sprintf("%s:%d", filename, f.Line)
			},
		})
	} else {

		logrus.SetFormatter(&logrus.TextFormatter{
			DisableColors:    false,
			FullTimestamp:    true,
			QuoteEmptyFields: true,
			ForceColors:      true,
			TimestampFormat:  "06-01-02 15:04:05.000",
			CallerPrettyfier: func(f *runtime.Frame) (string, string) {

				_, filename := path.Split(f.File)

				return fmt.Sprintf("%s:%-50d", filename, f.Line)[:30] + "|", " "
			},
		})
	}
}

func getLogLevel(logLevel string) logrus.Level {

	logLevel = strings.ToUpper(logLevel)

	if logLevel == "TRACE" {
		return logrus.TraceLevel
	}
	if logLevel == "DEBUG" {
		return logrus.DebugLevel
	}
	if logLevel == "INFO" {
		return logrus.InfoLevel
	}
	if logLevel == "WARN" {
		return logrus.WarnLevel
	}
	if logLevel == "ERROR" {
		return logrus.ErrorLevel
	}
	if logLevel == "FATAL" {
		return logrus.FatalLevel
	}
	return logrus.InfoLevel
}
